[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## EEG Stroop  Time-frequency transform

**Maintainer:** [Julian Q. Kosciessa](kosciessa@mpib-berlin.mpg.de)

## Code overview

**A_TFR_wholeSpectrum_CSD**

- CFD-transform
- wavelet-transform (5 cycle), 1 to 40 Hz, -1.5 s to 4.5 s

**B_plotTFRdata**
- plot average TFR across conditions and condition difference