restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);

    load([pn.FTout, IDs{id}, '_stroop_TFR_CSD.mat'], 'TFRwave_Pre', 'TFRwave_Post');
    
    TFRPre(id,:,:,:) = squeeze(nanmean(TFRwave_Pre.powspctrm,1));
    TFRPost(id,:,:,:) = squeeze(nanmean(TFRwave_Post.powspctrm,1));
    
end % ID

channels = 44:60;

%% Plot average TFR across conditions and condition difference

figure; 
subplot(1,2,1);
imagesc(TFRwave_Pre.time, TFRwave_Pre.freq, squeeze(nanmean(nanmean(TFRPost(:,channels,:,:),2),1)))
hold on; line([0,0],[41,0], 'Color', 'w','LineWidth', 2)
hold on; line([2,2],[41,0], 'Color', 'w','LineWidth', 2)
hold on; line([3,3],[41,0], 'Color', 'w','LineWidth', 2)
title('TFR Pre- & Post-Stroop')
subplot(1,2,2);
imagesc(TFRwave_Pre.time, TFRwave_Pre.freq, squeeze(nanmean(nanmean(TFRPost(:,channels,:,:)-TFRPre(:,channels,:,:),2),1)))
hold on; line([0,0],[41,0], 'Color', 'w','LineWidth', 2)
hold on; line([2,2],[41,0], 'Color', 'w','LineWidth', 2)
hold on; line([3,3],[41,0], 'Color', 'w','LineWidth', 2)
title('TFR Post minus Pre-Stroop')
