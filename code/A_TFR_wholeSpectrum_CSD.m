%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);

    condEEG = 'stroop1';
    
    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');

    %% CSD transform
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% wavelets

    freq_cfg = [];
    freq_cfg.channel     = 'all';
    freq_cfg.method      = 'wavelet';
    freq_cfg.width       = 5;
    freq_cfg.keeptrials  = 'yes';
    freq_cfg.output      = 'pow';
    freq_cfg.foi         = 1:1:40;
    freq_cfg.toi         = -1.5:0.05:4.5;
    TFRwave_Pre = ft_freqanalysis(freq_cfg, data);
    
    %% Post measurement
    
    condEEG = 'stroop2';
    
    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
    
    data = ft_scalpcurrentdensity(csd_cfg, data);

    TFRwave_Post    = ft_freqanalysis(freq_cfg, data);
    
    save([pn.FTout, IDs{id}, '_stroop_TFR_CSD.mat'], 'TFRwave_Pre', 'TFRwave_Post');
end % ID
